import { AbstractControl } from '@angular/forms';
import { AsyncValidatorFn } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, debounceTime, take, switchMap, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../service/authentication.service';
import { Injectable } from '@angular/core';

// Password Validator
export class PasswordValidation {

  static MatchPassword(AC: AbstractControl) {
      const password = AC.get('password').value; // to get value in input tag
      const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
      if (password !== confirmPassword) {
          AC.get('confirmPassword').setErrors({ MatchPassword: true });
      } else {
          return null;
      }
  }
}

// Async email validator

@Injectable({
  providedIn: 'root'
})
export class CustomEmailValidator {
  constructor(private EmailService: AuthenticationService) {}
  showLoader = false;

  isEmptyInputValue(value: any): boolean {
    return value === null || value.length === 0;
  }

  existingEmailValidator(initialEmail: string = ''): AsyncValidatorFn {
    return (
      control: AbstractControl
    ):
      | Promise<{ [key: string]: any } | null>
      | Observable<{ [key: string]: any } | null> => {
        console.log(control.value);
        if (this.isEmptyInputValue(control.value)) {
        return of(null);
      } else if (control.value === initialEmail) {
        return of(null);
      } else {
        return control.valueChanges.pipe(
          debounceTime(500),
          take(1),
          switchMap( () => {
            const payload = {
              campaignUuid: '46aa3270-d2ee-11ea-a9f0-e9a68ccff42a',
              data: {
                  email: control.value
              }
            };
            this.showLoader = true;
            return this.EmailService.ValidateEmail(payload)
              .pipe(
                map((user: any) =>
                  {
                    this.showLoader = false;
                    if (user.data.status === 'EXISTS') {
                      return {existingEmail: {value: true}};
                    }

                    return null;

                  }
                ),
                catchError( () => {
                  this.showLoader = false;
                  return of(null);
                })
              );
            }
          )
        );
      }
    };
  }
}
