import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordValidation, CustomEmailValidator } from './core/matchPassword.validation';
import { AuthenticationService } from './service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  errorOccurred = false;
  errorMessage: string;
  registrationForm: FormGroup;
  showLoader = false;
  @ViewChild('showSuccessMessage') showSuccessMessage: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private SignUpService: AuthenticationService,
    public customEmailValidator: CustomEmailValidator
    ){
    this.registrationForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
        this.customEmailValidator.existingEmailValidator()
      ],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {
      validator: PasswordValidation.MatchPassword // your validation method
    });
  }

  get firstName() {
    return this.registrationForm.get('firstName');
  }

  get lastName() {
    return this.registrationForm.get('lastName');
  }

  get email() {
    return this.registrationForm.get('email');
  }

  get password() {
    return this.registrationForm.get('password');
  }

  get confirmPassword() {
    return this.registrationForm.get('confirmPassword');
  }

  CreateUserAccount() {
    this.showLoader = true;
    const payload = {
      campaignUuid: '46aa3270-d2ee-11ea-a9f0-e9a68ccff42a',
      data: {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        email: this.email.value,
        password: this.password.value
      }
    };

    this.SignUpService.SignUp(payload).subscribe(
      () => {
        this.showLoader = false;
        this.registrationForm.reset();
        this.showSuccessMessage.nativeElement.click();
      },
      error => {
        this.showLoader = false;
        this.errorOccurred = true;
        console.log(error.error.errors);
        error.error.errors.forEach(
          err => {
            this.errorMessage = err.message;
          }
        )

      }
    );
  }

  userConfirmed(event) {
    if (event === 'confirmed') {
      this.CreateUserAccount();
    }
  }

}
