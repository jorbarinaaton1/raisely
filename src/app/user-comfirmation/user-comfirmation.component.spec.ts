import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComfirmationComponent } from './user-comfirmation.component';

describe('UserComfirmationComponent', () => {
  let component: UserComfirmationComponent;
  let fixture: ComponentFixture<UserComfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserComfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
