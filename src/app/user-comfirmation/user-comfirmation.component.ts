import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-comfirmation',
  templateUrl: './user-comfirmation.component.html',
  styleUrls: ['./user-comfirmation.component.scss']
})
export class UserComfirmationComponent implements OnInit {

  @Output() sendReponse = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  confirmSignup() {
    this.sendReponse.emit('confirmed');
  }

}
