import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscribable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient: HttpClient) { }

  public SignUp(payload: any): Subscribable<any>{
    const url = 'https://api.raisely.com/v3/signup';
    return this.httpClient.post(url, payload);
  }

  public ValidateEmail(EmailPayload: any) {
    const url = 'https://api.raisely.com/v3/check-user ';
    return this.httpClient.post(url, EmailPayload);
  }

}
